<?php

/*

Functions for my plugin

*/

/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	function woocommerce_coordinadora_method_init() {
		if ( ! class_exists( 'WC_Coordinadora' ) ) {
			class WC_Coordinadora extends WC_Shipping_Method {

				/**
				 * Constructor for your shipping class
				 *
				 * @access public
				 * @return void
				 */
				public function __construct() {
					$this->id = 'coordinadora';
					$this->method_title = __( 'Coordinadora' );
					$this->method_description = __( 'Plugin for Coordinadora Web Service.' );
					$this->init();
					$this->enabled = $this->settings['enabled'];
					$this->title = $this->settings['title'];
					$this->client = $this->settings['client'];
					$this->client_nit = $this->settings['client_nit'];
					$this->ws_gg_url = $this->settings['ws_gg_url'];
					$this->ws_gg_client_id = $this->settings['ws_gg_client_id'];
					$this->ws_gg_client_user = $this->settings['ws_gg_client_user'];
					$this->ws_gg_client_password = $this->settings['ws_gg_client_password'];
					$this->ws_sd_url = $this->settings['ws_sd_url'];
					$this->ws_sd_api_key = $this->settings['ws_sd_api_key'];
					$this->ws_sd_client_password = $this->settings['ws_sd_client_password'];
					$this->test = $this->settings['test'];
				}

				/**
				 * Init your settings
				 *
				 * @access public
				 * @return void
				 */
				function init() {
					$this->init_form_fields();
					$this->init_settings();
					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
				}

				/**
				 * Init settings form fields
				 *
				 * @access public
				 * @return void
				 */
				function init_form_fields() {
					$this->form_fields = array(
						'enabled' => array(
							'title' => __( 'Habilitar / Deshabilitar:', 'coordinadora' ),
							'type' => 'checkbox',
							'default' => 'no',
							'label' => __( 'Habilita el Web Service para Coordinadora.', 'coordinadora' )
						),
						'title' => array(
							'title' => __( 'Título:', 'coordinadora' ),
							'description' => __( 'Título que el usuario verá.', 'coordinadora' ),
							'type' => 'text',
							'default' => __( 'Coordinadora', 'coordinadora' )
						),
						'client' => array(
							'title' => __( 'Nombre del Cliente:', 'coordinadora' ),
							'description' => __( 'Nombre del cliente.', 'coordinadora' ),
							'type' => 'text'
						),
						'client_nit' => array(
							'title' => __( 'NIT del Cliente:', 'coordinadora' ),
							'description' => __( 'NIT del cliente.', 'coordinadora' ),
							'type' => 'text'
						),
						'ws_gg_url' => array(
							'title' => __( 'URL Generación de Guías:', 'coordinadora' ),
							'description' => __( 'URL del Web Service de generación de guías.', 'coordinadora' ),
							'type' => 'text',
							'default' => 'http://sandbox.coordinadora.com/agw/ws/guias/1.5/server.php?wsdl'
						),
						'ws_gg_client_id' => array(
							'title' => __( 'ID del Cliente:', 'coordinadora' ),
							'description' => __( 'ID único del cliente en Coordinadora para la generación de guías.', 'coordinadora' ),
							'type' => 'text'
						),
						'ws_gg_client_user' => array(
							'title' => __( 'Usuario del Cliente:', 'coordinadora' ),
							'description' => __( 'Usuario del cliente en Coordinadora para la generación de guías.', 'coordinadora' ),
							'type' => 'text'
						),
						'ws_gg_client_password' => array(
							'title' => __( 'Contraseña del Cliente:', 'coordinadora' ),
							'description' => __( 'Contraseña del cliente en Coordinadora para la generación de guías.', 'coordinadora' ),
							'type' => 'password'
						),
						'ws_sd_url' => array(
							'title' => __( 'URL Seguimiento de Despachos:', 'coordinadora' ),
							'description' => __( 'URL del Web Service de seguimiento de despachos.', 'coordinadora' ),
							'type' => 'text',
							'default' => 'http://sandbox.coordinadora.com/ags/1.4/server.php?wsdl'
						),
						'ws_sd_api_key' => array(
							'title' => __( 'API Key:', 'coordinadora' ),
							'description' => __( 'Llave que sirve para encriptar la comunicación con Coordinadora para el seguimiento de despachos.', 'coordinadora' ),
							'type' => 'text'
						),
						'ws_sd_client_password' => array(
							'title' => __( 'Contraseña del Cliente:', 'coordinadora' ),
							'description' => __( 'Contraseña del cliente en Coordinadora para el seguimiento de despachos.', 'coordinadora' ),
							'type' => 'password'
						),
						'test' => array(
							'title' => __( 'Envíos en Modo de Prueba:', 'coordinadora' ),
							'type' => 'checkbox',
							'default' => 'no',
							'label' => __( 'Habilita los envíos en modo de prueba.', 'coordinadora' )
						),
					);
				}

				/**
				 * Displaying admin options
				 *
				 * @access public
				 * @return void
				 */
				function admin_options() {
					?>
					<h2><?php _e( 'Coordinadora', 'coordinadora' ); ?></h2>
					<table class="form-table">
						<?php $this->generate_settings_html(); ?>
					</table>
					<?php
				}

			}
		}
	}
	add_action( 'woocommerce_shipping_init', 'woocommerce_coordinadora_method_init' );

	function woocommerce_coordinadora_method( $methods ) {
		$methods['coordinadora'] = 'WC_Coordinadora';
		return $methods;
	}
	add_filter( 'woocommerce_shipping_methods', 'woocommerce_coordinadora_method' );

}